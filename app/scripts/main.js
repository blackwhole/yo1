console.log('\'Allo \'Allo!');

var app = angular.module('myapp', ['ui.bootstrap']); // ui.bootstrap dependency - without it, nothing resolves

app.controller('HelloCtr', function($scope) {
    //$scope.helloTo = {};
    //$scope.helloTo.title = 'I got Angular working';
    $scope.helloMessage = "Compliant";

  } );

app.controller('acd', function($scope) {

  $scope.oneAtATime = true;

  $scope.groups = [{
    title: 'Dynamic Group Header - 1',
    content: 'Dynamic Group Body - 1'
  }, {
    title: 'Dynamic Group Header - 2',
    content: 'Dynamic Group Body - 2'
  }];

  $scope.items = ['Item 1', 'Item 2', 'Item 3'];

  $scope.addItem = function () {
    var newItemNo = $scope.items.length + 1;
    $scope.items.push('Item ' + newItemNo);
  };

  console.log('ran controller acd');
});


